import glob
import os
import sys
import pandas as pd

sys.path.insert(0, os.path.abspath("./src"))
from extractive import ExtractiveSummarizer  # noqa: E402

text_to_summarize = '''One of the Catholic Church's most senior members has been arrested in Hong Kong for breaking China's national security law, police have confirmed.Cardinal Joseph Zen, 90, is one of four people detained for being associated with a now-defunct organisation that helped protesters in financial need.The others are a Cantopop singer and actor Denise Ho, ex-legislator Margaret Ng, and academic Dr Hui Po Keung.They are accused of colluding with foreign forces.If found guilty, they could face life in prison. Human Rights Watch called it a "shocking new low for Hong Kong."Hong Kong Police told the BBC that the group was suspected of appealing to foreign countries or organisations to impose sanctions on Hong Kong, hence threatening China's national security.
Cardinal Zen fled Shanghai for Hong Kong after the communists took over China 70 years ago, and is a former bishop of Hong Kong. He has long been a critic of the government in Beijing, speaking out for Catholics in mainland China and for more democracy in Hong Kong.
He once publicly admonished the Vatican for "selling out" to China by forcing bishops to retire in favour of replacements picked by Beijing.
"Arresting a 90-year-old cardinal for his peaceful activities has to be a shocking new low for Hong Kong, illustrating the city's free fall in human rights in the past two years," Human Rights Watch said.
The Vatican is concerned about the cardinal's arrest, spokesman Matteo Bruni said in a statement.
The Catholic Diocese of Hong Kong has also spoken out, saying they are "extremely concerned about the condition and safety of Cardinal Joseph Zen".
"We urge the Hong Kong Police and the judicial authorities to handle Cardinal Zen's case in accordance with justice," they added.
Dr Hui, a scholar with Hong Kong's Lingnan University, was arrested at the airport as he tried to fly to Europe to take up an academic posting, Hong Kong Free Press reports, citing two legal sources.
This is the second time Denise Ho has been arrested in as many months - she was detained late last year under the same law.
Margaret Ng has also been arrested in the past - in 2021 she was handed a one year suspended sentence for participating in unauthorised demonstrations. During the hearing, the barrister dismissed her own lawyer and gave such a rousing speech, the court erupted into applause.
Hong Kong Police told the BBC the four defendants would be released on bail, but must hand over their passports.
They are believed to have been associated with the 612 Humanitarian Relief Fund, which helped pro-democracy protesters pay their legal and medical fees.
Cardinal Zen is someone many Catholics in Hong Kong look up to. He's been seen marching with citizens in protests, and more recently attending court hearings of pro-democracy activists. Some affectionately call him "Grandpa Cardinal".
One political pundit, Dr Simon Shen, says his arrest will seriously affect relations between China and the Vatican.
The Roman Catholic Church has been seeking to improve its relations with Beijing for decades - something Cardinal Zen is not happy about, as he says the rights of Christians in Mainland China are not being looked after in their negotiations.
Meanwhile, Beijing's official newspapers in Hong Kong call the Cardinal a "lawless" anti-China person.
Just after the arrests were made known to the public, one paper published what it calls "evidence" of the 612 Fund's "collusion" against China.
His arrest will inevitably become a yet another sticking point in fraying relations between Hong Kong and the international community, driven by the Beijing-imposed National Security Law.
The organisation was disbanded last year after the national security police demanded it hand over sensitive information including details about its members and donors.
Scores of pro-democracy activists and protesters in Hong Kong have been arrested under the national security law since it was imposed by China in 2020.
It essentially bans sedition, secession and treason, and therefore makes it easier for authorities to crack down on protesters and punish them.
The arrests come days after the Chinese government appointed a new hard-line pro-Beijing leader for Hong Kong, John Lee Ka-chiu.
"The arrests... [are] an ominous sign that its crackdown on Hong Kong is only going to escalate," Human Rights Watch said.
'''

model = ExtractiveSummarizer.load_from_checkpoint("./distilroberta-base.ckpt",strict=False)

summary = model.predict(text_to_summarize,num_summary_sentences=5,raw_scores=True)

outs = sorted(summary,key=lambda x:x[1], reverse=True)

# print(outs)

sentences = []
scores = []
for ele in outs:
    sentences.append(ele[0])
    scores.append(ele[1])


df = pd.DataFrame({'Sentence':sentences,'Scores':scores})

print(df)

with open("prediction_distilroberta.txt", "w") as outfile:
    outfile.write(df.to_markdown())
